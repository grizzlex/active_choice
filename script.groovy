properties([parameters([
    [
        $class: 'ChoiceParameter',
        choiceType: 'PT_SINGLE_SELECT',
        name: 'BUILD_MTC2_Cluster',
        description: 'Which MTC2 cluster will this be provisioned nad installed on?',
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [
                classpath: [], sandbox: true, script: ''
            ],
            script: [   
                classpath: [], sandbox: true, script:'return ["true","false"]'

            ]
        ]
    ]
    
])])

pipeline {
    agent any
    
    stages{
        stage("build") {
            steps{
                echo 'buliding the application'
            }
        }
        stage("test"){
            steps {
                echo 'testing the application'
            }
        }
        stage("deploy"){
            steps {
                echo 'deploying teh application'
            }
        }
        
    }

 }
    