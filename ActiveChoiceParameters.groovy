properties([parameters([
    [
        $class: 'ChoiceParameter',
        choiceType: 'PT_SINGLE_SELECT',
        name: 'BUILD_CLUSTER_NAME',
        description: 'Which MTC2 cluster will this be provisioned and installed on?',
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [
                classpath: [], sandbox: true, script: ''
            ],
            script: [   
                classpath: [], sandbox: true, script:'return ["aws_env_b3","aws_env_b","aws_env_c3","aws_env_e"]'
            ]
        ]
    ],
    [   
        $class: 'ChoiceParameter',
        choiceType: 'PT_SINGLE_SELECT', 
        description: 'Destroy the currently deployed cluster and reload the OSs?', 
        filterLength: 1, 
        filterable: false, 
        name: 'DESTRUCTIVE_LOAD', 
        randomName: 'choice-parameter-1221311170216428', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [
                classpath: [], sandbox: false, script: 'return'
                ], 
                script: [
                    classpath: [], sandbox: false, script: 'return ["true","false"]'
                ]
        ]
    ],
    [
        $class: 'CascadeChoiceParameter',
        choiceType: 'PT_SINGLE_SELECT', 
        description: '''Which AWS VPC should this be installed in?
        mtc2-prod   :    The standard VPC that most of MTC2 clusters are in;
        mtc2-devtest:    The other VPC that has additional IP space''',
        filterLength: 1, 
        filterable: false, 
        name: '​​​​​​​​    • AWS_VPC', 
        randomName: 'choice-parameter-1222985705858462', 
        referencedParameters: 'DESTRUCTIVE_LOAD', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (DESTRUCTIVE_LOAD.equals("true")) { return["mtc2-prod", "mtc2-devtest"] } else if (DESTRUCTIVE_LOAD.equals("false")) { return }'
            ]
        ]
    ],
    [
        $class: 'CascadeChoiceParameter', 
        choiceType: 'PT_SINGLE_SELECT',
        description: '''Which AWS nightly shutdown option should be used for the cluster? Defaults to standard.
        Standard:   The cluster is powered off at 19:00;
        Ignore:     The cluster is not powered off at night. Please only use as necessary!''',
        filterLength: 1, 
        filterable: false, 
        name: '​​​​​​​​    • NIGHTLY_SHUTDOWN', 
        randomName: 'choice-parameter-1223303760296943', 
        referencedParameters: 'DESTRUCTIVE_LOAD', 
            script: [
                $class: 'GroovyScript', 
                fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
                script: [
                    classpath: [], sandbox: false, script: 'if (DESTRUCTIVE_LOAD.equals("true")) { return["standard", "ignore"] } else if (DESTRUCTIVE_LOAD.equals("false")) { return }'
                ]
            ]
    ],
    [
        $class: 'DynamicReferenceParameter', 
        choiceType: 'ET_FORMATTED_HTML',
        description: 'Enter the AMI to use during terraform. Defaults to ami-e98eca88', 
        name: 'AMI', 
        omitValueField: false, 
        randomName: 'choice-parameter-1223517583414604', 
        referencedParameters: 'DESTRUCTIVE_LOAD', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (DESTRUCTIVE_LOAD.equals("true")) { return "<input type=\\"text\\" name=\\"value\\" value=\\"ami-e98eca88\\" />" } else { return }'
            ]
        ]
    ],
    [
        $class: 'ChoiceParameter', 
        choiceType: 'PT_SINGLE_SELECT', 
        description: 'Pull the selected branch for Ansible playbooks from MTC2-infrastructure, prep the inventory file and Ansible working directory', 
        filterLength: 1, 
        filterable: false, 
        name: 'PREP_ANSIBLE', 
        randomName: 'choice-parameter-1223727115884066', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'return ["true","false"]'
            ]
        ]
    ],
    [
        $class: 'DynamicReferenceParameter', 
        choiceType: 'ET_FORMATTED_HTML', 
        description: 'Enter the branch name, tag, or commit hash to be checked out for the mtc2-infrastructure Ansible playbooks. Defaults to the develop branch', 
        name: 'ANSIBLE_BRANCH', 
        omitValueField: false, 
        randomName: 'choice-parameter-1223887815804223', 
        referencedParameters: 'PREP_ANSIBLE', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (PREP_ANSIBLE.equals("true")) { return "<input type=\\"text\\" name=\\"value\\" value=\\"develop\\" />" } else { return }'
            ]
        ]
    ],
    [
        $class: 'ChoiceParameter', 
        choiceType: 'PT_SINGLE_SELECT', 
        description: 'Download latest artifacts and stage them under PIPELINE', 
        filterLength: 1, 
        filterable: false, 
        name: 'STAGE_ARTIFACTS', 
        randomName: 'choice-parameter-1224007161323654', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'return ["true","false"]'
            ]
        ]
    ],
    [
        $class: 'DynamicReferenceParameter', 
        choiceType: 'ET_FORMATTED_HTML',
        description: 'Enter the branch name to use for staged artifacts', 
        name: 'PACKAGE_BRANCH', 
        omitValueField: false, 
        randomName: 'choice-parameter-1224123958966156', 
        referencedParameters: 'STAGE_ARTIFACTS',
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (STAGE_ARTIFACTS.equals("true")) { return "<input type=\\"text\\" name=\\"value\\" value=\\"develop\\" />" } else { return }'
            ]
        ]
    ],
    [
        $class: 'ChoiceParameter', 
        choiceType: 'PT_SINGLE_SELECT', 
        description: 'Run Ansible MTC2_install.yml playbook', 
        filterLength: 1,
        filterable: false, 
        name: 'RUN_ANSIBLE', 
        randomName: 'choice-parameter-1224359769462492', 
        script: [
            $class: 'GroovyScript',
            fallbackScript: [classpath: [], sandbox: false, script: 'return'],
            script: [
                classpath: [], sandbox: false, script: 'return ["true","false"]'
            ]
        ]
    ],
    [
        $class: 'CascadeChoiceParameter', 
        choiceType: 'PT_SINGLE_SELECT',
        description: 'Install GCCS-M Components',
        filterLength: 1, 
        filterable: false, 
        name: 'GCCSM', 
        randomName: 'choice-parameter-1224359771344891',
        referencedParameters: 'RUN_ANSIBLE', 
        script: [
            $class: 'GroovyScript',
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (RUN_ANSIBLE.equals("true")) { return["true", "false"] } else if (RUN_ANSIBLE.equals("false")) { return }'
            ]
        ]
    ],
    [
        $class: 'CascadeChoiceParameter',
        choiceType: 'PT_SINGLE_SELECT',
        description: 'Install TPT Components', 
        filterLength: 1,
        filterable: false, 
        name: 'TPT', 
        randomName: 'choice-parameter-1224618173621202', 
        referencedParameters: 'RUN_ANSIBLE', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (RUN_ANSIBLE.equals("true")) { return["true", "false"] } else if (RUN_ANSIBLE.equals("false")) { return }'
            ]
        ]
    ], 
    [
        $class: 'DynamicReferenceParameter', 
        choiceType: 'ET_FORMATTED_HTML',
        description: 'Enter the package version to use for the Ansible installation. This will be appended on to the --extra-vars parameters for all packages, e.g. opt_gui_version=2018-RC1 if you entered 2018-RC1',
        name: 'PACKAGE_VERSION',
        omitValueField: false, 
        randomName: 'choice-parameter-1224618175470159', 
        referencedParameters: 'RUN_ANSIBLE', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'if (RUN_ANSIBLE.equals("true")) { return "<input type=\\"text\\" name=\\"value\\" value=\\"PIPELINE\\" />" } else { return }'
            ]
        ]
    ], 
    [
        $class: 'ChoiceParameter',
        choiceType: 'PT_SINGLE_SELECT',
        description: 'Run scripts necessary for performing RDTE ACAS scans',
        filterLength: 1, 
        filterable: false, 
        name: 'CONFIGURE_ACAS',
        randomName: 'choice-parameter-1224618177355789', 
        script: [
            $class: 'GroovyScript', 
            fallbackScript: [classpath: [], sandbox: false, script: 'return'], 
            script: [
                classpath: [], sandbox: false, script: 'return ["true","false"]'
            ]
        ]
    ],
])
])

    
    